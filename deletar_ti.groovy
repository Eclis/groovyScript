@GrabConfig(systemClassLoader = true)
@Grab('org.ale:gale:0.18')
@GrabExclude('org.codehaus.groovy:groovy-all')
conn = ale.Sqls.&withS2H

tabelasLigadasComTi = [
	"TBL_ACIONAMENTOS_TI": "ACI_TI",
	"TBL_CATEGORIAS_TI": "CTI_TI",
	"TBL_HISTORICOS_TI": "HTI_TI",
	"TBL_PARADASRELOGIO_TI": "RLI_TI",
	"TBL_PROCEDIMENTOS_TI": "PCI_TI",
	"TBL_TI_BAIXAS": "TIX_TI",
	"TBL_VIDAS_TI": "VDI_TI",
	"TBL_PROCEDIMENTO_CLIENTE": "PRC_TI",
	"TBL_PROCEDIMENTOS_SAP": "PCS_TI",
	"TBL_TI_ABERTURA": "TIA_TI",
	"TBL_ANEXOS_TI": "ANI_TI",
	"TBL_TI_CSO_REPAROS": "CSR_TI",
	"TBL_TI_SAGRE_DDR": "TDD_TI",
	"TBL_TI_INTRAGOV_OPERACOES": "IOP_TI"
]

tabelasLigadasComServico = [
	"TBL_TI_ADAPTADORES": "TIA_SERVICO",
	"TBL_TI_CABOSPAR": "TIP_SERVICO",
	"TBL_TI_CANALIZACOES": "TIL_SERVICO",
	"TBL_TI_ENDERECOS": "TIE_SERVICO",
	"TBL_TI_ENTRONCAMENTOS": "TIN_SERVICO",
	"TBL_TI_DETERMINISTICA": "TIR_SERVICO",
	"TBL_TI_DG": "TIG_SERVICO",
	"TBL_TI_FRAME": "TIF_SERVICO",
	"TBL_TI_IP": "TIJ_SERVICO",
	"TBL_TI_MODENS": "TIM_SERVICO",
	"TBL_TI_OPERADORAS": "TIQ_SERVICO",
	"TBL_TI_ROTEADORES": "TIO_SERVICO",
	"TBL_TI_TOPOLOGIA": "TIU_SERVICO",
	"TBL_TI_OPERADORA_EDIT": "EIQ_SERVICO",
	"TBL_TI_CANALIZACAO_FENIX": "TCF_SERVICO",
	"TBL_TI_METROLAN": "TIH_SERVICO",
	"TBL_TI_SAP_MODENS": "TSM_SERVICO",
	"TBL_TI_SAP_ROTEADORES": "TSR_SERVICO",
	"TBL_TI_RESTRICOES": "TRS_SERVICO",
	"TBL_TI_HDSL": "THD_SERVICO",
	"TBL_COMPONENTES_OPERADORA": "CMO_TI_SERVICOS",
	"TBL_TI_FIBRA": "TFB_SERVICO",
	"TBL_TI_GESTAO": "TGT_SERVICO",
	"TBL_TI_IDENTIFICACAO": "TID_SERVICO",
	"TBL_TI_RADIO": "TRD_SERVICO",
	"TBL_TI_RAMAIS": "TRL_SERVICO",
	"TBL_TI_ACESSO_UM": "TCS_SERVICO",
	"TBL_TI_APARELHOS": "TAP_SERVICO",
	"TBL_TI_COMPONENTE": "TIC_SERVICO",
	"TBL_TI_FACILIDADES": "TFC_SERVICO",
	"TBL_TI_RAMAIS_PORTADOS": "TRP_SERVICO",
	"TBL_METROLAN_LP": "MET_SERVICOS",
	"TBL_GPON": "GPN_TI_SERVICOS",
	"TBL_FENIX_SISTEMA": "TFS_SERVICO",
	"TBL_FENIX_SISTEMA_SUP": "TSS_SERVICO",
	"TBL_FENIX_EQUIPAMENTO": "TFE_SERVICO",
	"TBL_FENIX_PLACA": "TFP_SERVICO",
	"TBL_FENIX_DISTRIBUIDOR": "TFD_SERVICO"
]

conn { sql ->
    def query = 'SELECT TQI_CODIGO FROM TBL_TI WHERE TQI_CODIGO > 2501628 AND TQI_CODIGO NOT IN (2501630, 2501657)'
    sql.eachRow(query) { row ->
    	tabelasLigadasComTi.each { tabelaTi, colunaTi ->
    		sql.execute("DELETE FROM " + tabelaTi +" WHERE " + colunaTi + " = ${row.tqi_codigo}")
    	}

    	Long tisCodigo = sql.firstRow("SELECT TIS_CODIGO FROM TBL_TI_SERVICOS WHERE TIS_TI = ${row.tqi_codigo}")?.tis_codigo
    	if (tisCodigo) {
    		tabelasLigadasComServico.each { tabelaServico, colunaServico ->
    			sql.execute("DELETE FROM " + tabelaServico +" WHERE " + colunaServico + " = ${tisCodigo}")
    		}
    		sql.execute("DELETE FROM TBL_TI_SERVICOS WHERE TIS_CODIGO = ${tisCodigo}")
    	}
    	sql.execute("DELETE FROM TBL_TI WHERE TQI_CODIGO = ${row.tqi_codigo}")
    	println "DELETADO TI: $row.tqi_codigo"
    }
}