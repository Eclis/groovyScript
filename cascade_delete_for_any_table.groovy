@GrabConfig(systemClassLoader = true)
@Grab('org.ale:gale:0.18')
@GrabExclude('org.codehaus.groovy:groovy-all')
conn = ale.Sqls.&withS2H

alreadyCheckedTables = [:]

conn { sql ->
	recursiveDeleteContentOfSourceTable(sql, 'TBL_TI', 'TQI_CODIGO', [2501710])
}

def recursiveDeleteContentOfSourceTable(sql, String table, String column, value) {
    if (!value) return
    alreadyCheckedTables."$table" = value
    def querySourceTables = "SELECT * FROM V_FKS WHERE TARGET_TABLE = ${table}"
    sql.eachRow(querySourceTables) { row ->
        def codigoColumn = "${row.SOURCE_COLUMN[0..2]}_CODIGO"
        def queryCodigo = "SELECT $codigoColumn AS CODIGO FROM $row.SOURCE_TABLE WHERE $row.SOURCE_COLUMN IN (${value.join(',')})".toString() 
        //println "$queryCodigo;"
        def codigos = sql.rows(queryCodigo)
        if(alreadyCheckedTables.containsKey(row.SOURCE_TABLE)) {
            def queryAvoidCycleReference = "SELECT * FROM V_FKS WHERE SOURCE_TABLE = $row.SOURCE_TABLE AND TARGET_TABLE = $table"
            //println "TRETA TA AQUI $table , $column e $value com a tabela $row.SOURCE_TABLE"
            def cycleTable = sql.firstRow(queryAvoidCycleReference)
            def cycleValue = alreadyCheckedTables."$row.SOURCE_TABLE".join(',')
            println "UPDATE $cycleTable.SOURCE_TABLE SET $cycleTable.SOURCE_COLUMN = NULL WHERE $codigoColumn IN ($cycleValue);"
            return
        } else {
            alreadyCheckedTables."$row.SOURCE_TABLE" = codigos.codigo.join(',')
        }
        recursiveDeleteContentOfSourceTable(sql, row.SOURCE_TABLE, codigoColumn, codigos.codigo)
    }
    println "DELETE FROM $table WHERE $column IN (${value.join(',')});"
}