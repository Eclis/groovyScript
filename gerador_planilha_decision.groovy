@Grab(group='org.apache.poi', module='poi', version='3.15')

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import groovy.time.TimeCategory
import java.util.Calendar

cal = Calendar.getInstance();
cal.set(Calendar.DAY_OF_MONTH, 1)
cal.set(Calendar.MONTH, 10)
date = cal.getTime()

map = [
    0:[hours:8, plusHours: 0, minutes:45, module:30],
    1:[hours:0, plusHours: 3, minutes:0, module:5],
    2:[hours:0, plusHours: 1, minutes:0, module:5],
    3:[hours:0, plusHours: 5, minutes:0, module:5]
]

new HSSFWorkbook().with { workbook ->
  createSheet( 'Output' ).with { sheet ->
        (0..27).each { rownum ->
            createRow( rownum ).with { row ->
                (0..3).each { colnum ->
                    createCell( colnum ).with { cell -> 
                        int dow = cal.get (Calendar.DAY_OF_WEEK);
                        boolean isWeekday = ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));
                        if (isWeekday) {
                            setCellValue( horario(colnum) ) 
                        }
                    }
                }
                cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1)
            }
        }
    new File( '/tmp/test.xls' ).withOutputStream { os ->
      write( os )
    }
  }
}

def random(minutes, module) {
    def random = minutes + Math.abs(new Random().nextInt() % module)
    return random
}

def doCalc(Integer hours, plusHours, minutes, module) {
    use (TimeCategory) {
        def random = random(minutes, module)
        if (hours) {
            date.hours = hours
            date.minutes = random
            date.seconds = 0
        } else {
            date = date + plusHours.hours + random.minutes
        }
        println "${date.hours.toString().padLeft(2, '0')}:${date.minutes.toString().padLeft(2, '0')}:00"
        return "${date.hours.toString().padLeft(2, '0')}:${date.minutes.toString().padLeft(2, '0')}:00"
    }
}

def horario(colnum) {
    def cell = map.get(colnum)
    return doCalc(cell.hours, cell.plusHours, cell.minutes, cell.module)
}

return
